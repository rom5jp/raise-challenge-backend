require 'correios-cep'

class Api::V1::AddressesController < ApplicationController
  before_action :set_address, only: [:show, :update, :destroy]
  
  respond_to :json

  def show
    begin
      if @address.nil?
        

        
      end

      respond_with @address
    rescue
      head 404
    end
  end

  def create
    address = Address.new(address_params)

    if address.save
      render json: address, status: 201
    else
      render json: { errors: address.errors }, status: 422
    end
  end

  def update
    if @address.update(address_params)
      respond_with @address
    else
      render json: { errors: address.errors }, status: 422
    end
  end

  def destroy
    @address.destroy
    head 204
  end

  def retrieve_by_cep_from_correios
    begin
      address = retrieve_from_correios(extra_params[:cep])
      respond_with address
    rescue
      head 404
    end
  end

  def retrieve_by_street
    addresses = Address.new.retrieve_multiple(params[:street])
    render json: addresses, status: 200
  end

  private

  def address_params
    params.require(:address).permit(
      :street, 
      :neighborhood, 
      :city, 
      :state, 
      :zipcode, 
      :complement
    )
  end

  def extra_params
    params.permit(
      :cep
    )
  end

  def set_address
    @address = User.where(email: params[:id])
  end

  def purify_cep(cep)
    cep.gsub('-', ' ')
  end

  def retrieve_from_correios(cep)
    cep = purify_cep(params[:cep])
    Correios::CEP::AddressFinder.new.get(cep)
  end
end
