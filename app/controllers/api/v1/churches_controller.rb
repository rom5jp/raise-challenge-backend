class Api::V1::ChurchesController < ApplicationController
  before_action :set_church, only: [:show, :update, :destroy]
  
  respond_to :json

  def index
    @churches = User.all
    respond_with @churches
  end
  
  def show
    begin
      @church = Church.find(params[:id])
      respond_with @church
    rescue
      head 404
    end
  end

  def create
    church = Church.new(church_params)

    if church.save
      render json: church, status: 201
    else
      render json: { errors: church.errors }, status: 422
    end
  end

  def update
    @church = params[:church]

    if @church.update(church_params)
      render json: @church, status: 200
    else
      render json: { errors: @church.errors }, status: 422
    end
  end

  def destroy
    current_church.destroy
    head 204
  end

  private

  def church_params
    params.require(:church).permit(
      :name, 
      :leader_name
    )
  end

  def set_church
    @church = Church.where(church_name: church_params[:name])
  end
end
