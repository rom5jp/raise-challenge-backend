class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  respond_to :json

  def index
    @users = User.all
    respond_with @users
  end

  def show
    respond_with @user
  end

  def create
    user = User.new(user_params)

    if user.save
      render nothing: true, status: 200
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def update
    if @user.update(user_params)
      render json: @user, status: 200
    else
      render json: { errors: @user.errors }, status: 422
    end
  end

  def destroy
    @user.destroy
    head 204
  end

  private

  def user_params
    params.require(:user).permit(
      :email,
      :name, 
      :surname, 
      :cpf, 
      :cellphone, 
      :homephone, 
      :whatsapp_cellphone, 
      :church, 
      :church_member
    )
  end

  def set_user_by_email_or_cpf
    begin
      if user_params[:email].present?
        @user = User.find_by(email: user_params[:email])
      elsif user_params[:cpf].present?
        @user = User.find_by(cpf: user_params[:cpf])
      end
    rescue
      head 404
    end
  end

  def set_user
    begin
      @user = User.find(params[:id])
    rescue
      head 404
    end
  end
end
