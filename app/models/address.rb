class Address < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :church, optional: true

  def retrieve_multiple(street)
    results = []
    street.split(' ').each do |word|
      results << Address.where('lower(street) LIKE ?', "%#{word.downcase}%")
    end

    results.flatten.uniq
  end
end
