class Church < ApplicationRecord
  has_one :address
  has_many :users

  validates_uniqueness_of :name
  validates_presence_of :name, :leader_name
end
