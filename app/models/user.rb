class User < ApplicationRecord
  has_many :addresses
  belongs_to :church, optional: true

  validates_uniqueness_of :email, :cpf
  validates_presence_of :name, :email, :cpf
end
