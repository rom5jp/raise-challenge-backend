require 'api_version_constraint'

# configure your 'hosts' file to: 
# 127.0.0.1   api.raise-challenge.dev

Rails.application.routes.draw do
  namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api'}, path: '/' do
    namespace :v1, path: '/', constraints: ApiVersionConstraint.new(version: 1, default: true) do
      resources :users, only: [:index, :show, :create, :update, :destroy] do
        
      end

      resources :addresses, only: [:index, :show, :create, :update, :destroy] do
      end
      
      match '/addresses/retrieve_by_cep_from_correios/:cep', controller: 'addresses', action: 'retrieve_by_cep_from_correios', via: 'get'
      match '/addresses/retrieve_by_street', controller: 'addresses', action: 'retrieve_by_street', via: 'post'
    end
  end
end
