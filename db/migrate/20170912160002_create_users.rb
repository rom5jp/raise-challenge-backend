class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :name
      t.string :surname
      t.string :cpf
      t.string :cellphone
      t.string :homephone
      t.boolean :whatsapp_cellphone
      t.boolean :church_member
      t.belongs_to :church, index: { unique: true }, foreign_key: true

      t.timestamps
    end

    add_index :users, :email, unique: true
  end
end