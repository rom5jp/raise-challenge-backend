class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :neighborhood
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :complement
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :church, index: true, foreign_key: true

      t.timestamps
    end
  end
end