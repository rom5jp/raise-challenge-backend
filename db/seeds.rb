# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

church = Church.create!(
  name: 'IPT',
  leader_name: 'Robinson Grangeiro'
)

user = User.create!(
  name: 'Romero',
  surname: 'Medeiros',
  email: 'romeromedeiros5@gmail.com',
  cpf: '07644271481',
  church: church
)

user_address = Address.create!(
  street: 'Rua Antonio Rabelo Jr',
  neighborhood: 'Miramar',
  city: 'Joao Pessoa',
  state: 'PB',
  zipcode: '58032090',
  complement: 'Apto 1002',
  user: user,
  church: nil
)

church_address = Address.create!(
  street: 'Rua da IPT',
  neighborhood: 'Manaíra',
  city: 'João Pessoa',
  state: 'PB',
  zipcode: '58032000',
  church: church
)