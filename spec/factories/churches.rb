FactoryGirl.define do
  factory :church do
    name { Faker::LordOfTheRings.location }
    leader_name { Faker::LordOfTheRings.character }
  end
end