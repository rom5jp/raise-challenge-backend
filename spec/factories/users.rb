FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    name { Faker::Name.first_name }
    surname { Faker::Name.last_name }
    cellphone { Faker::PhoneNumber.cell_phone }
    homephone { Faker::PhoneNumber.phone_number }
    cpf { Faker::Number.number(11) }
  end
end