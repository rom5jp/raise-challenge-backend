require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:cpf) }
  it { is_expected.to validate_uniqueness_of_of(:email).case_insensitive }
  it { is_expected.to validate_uniqueness_of_of(:cpf).case_insensitive }
end
