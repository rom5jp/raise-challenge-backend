require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  let!(:user) { create(:user)}
  let(:user_id) { user.id}

  describe "GET /user/:id" do
    before do
      headers = { "Accept" => "application/vnd.raise.v1" }
      get "/ujsers/#{user_id}", {}, headers
    end

    context "when the user exists" do
      user_response = JSON.parse(response.body)
      expect(user_response["id"]).to eq(user_id)
    end
  end
end